import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

const vuexLocal = new VuexPersistence({
    storage: window.localStorage
})

Vue.use(Vuex)

export default new Vuex.Store({
    plugins: [vuexLocal.plugin],
    state: {
        userId: '',
        bloodTypeFilter: ''
    },
    mutations: {
        login: function (state, userId) {
            state.userId = userId
        },
        filterBloodType: function (state, bloodType) {
            state.bloodTypeFilter = bloodType
        },
        logout: function (state) {
            state.userId = ''
        }
    },
    getters: {
        getBloodTypeFilter: state => state.bloodTypeFilter,
        userId: state => state.userId
    },
    actions: {}
})
