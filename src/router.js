import Vue from 'vue'
import Router from 'vue-router'
import store from './store'

Vue.use(Router)

const needAuth = function (to, from, next) {
    if (store.getters.userId === '') {
        next('/')
    }
    next()
}

export default new Router({
    routes: [
        {
            path: '/',
            name: 'login',
            component: () => import ('./components/login-page')
        },
        {
            path: '/profiler',
            component: () => import ('./components/profiler-page'),
            beforeEnter: needAuth,
            children: [
                {
                    path: 'profiling-donor',
                    component: () => import ('./components/profiling-donor-page')
                },
                {
                    path: 'donor-list',
                    component: () => import('./components/donor-list-page')
                }
            ]
        }
    ]
})
